package com.example.stackedbar;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    BarChart stackedChart;
    int[] colorClassArray = new int[]{Color.RED, Color.CYAN,Color.MAGENTA};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        stackedChart = findViewById(R.id.stackedBar);

        BarDataSet barDataSet = new BarDataSet(dataValues(),"BarSet");
        barDataSet.setColors(colorClassArray);
        barDataSet.setValueTextSize(10);


        BarData barData = new BarData(barDataSet);
        stackedChart.setData(barData);
    }
    private ArrayList<BarEntry> dataValues(){
        ArrayList<BarEntry> dataVals = new ArrayList<>();
        dataVals.add(new BarEntry(0, new float[]{2,5.5f,4}));
        dataVals.add(new BarEntry(1, new float[]{2,8f,5.3f}));
        dataVals.add(new BarEntry(2, new float[]{2,3,8}));
        return dataVals;

    }
}
